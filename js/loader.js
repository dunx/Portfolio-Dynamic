$(document).ready(function() {
  $("#content").load(); //loads nothing to start
})

$('a.dynamic').click(function(){ //only dynamically loads content with 'dynamic' class within a tag, all other links open normally, external links open in new tab
  var page = $ (this).attr('href');
  $("#content").empty(); //empties current content
  $("#content").load(page); //loads new content under 'page' variable

  return false;
})
